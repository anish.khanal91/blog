const Article = require('./../models/article')


exports.addBlog = async (req,res)=>{
    let article = new Article({
        title :req.body.title,
        description :req.body.description,
        markdown :req.body.markdown,
    })
    try{
       article =  await article.save()
       res.redirect(`/articles/${article.id}`)
    
    }catch(e){
        res.render('articles/new', {article :article})
    }
    }

exports.returnBlog = (req,res)=>{
    res.render('./articles/new',{article: new Article()})
}
exports.deleteblog = async (req, res)=> {
    await Article.findByIdAndDelete(req.params.id)
    res.redirect("/")
}

exports.showBlog = async (req,res)=>{
    const article = await Article.findById(req.params.id)
    if(article ==null)res.redirect('/')
        res.render('articles/show', {article:article})
}

exports.editBlog = async (req, res) => {
    let article;
    try {
        article = await Article.findById(req.params.id)
        const { title, description, markdown } = req.body
        article.title = title
        article.description = description
        article.markdown = markdown
        await article.save();
        res.redirect(`/articles/${req.params.id}`)

    } catch (e) {
        res.render("articles/edit", { article: article })
    }
}